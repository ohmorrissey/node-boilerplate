FROM node:18-alpine

COPY [ "./dist/", "./" ]

COPY [ "./node_modules/", "./node_modules/" ]

EXPOSE 8080

CMD ["node", "src/index.js"]