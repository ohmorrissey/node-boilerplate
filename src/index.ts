import configRaw from "../config/config.json";
import express from "express";

const config = configRaw as any;

let message = "Hello without Config";

if (config.message){
    message = config.message;
}

const app = express();

app.get('/', (req, res) => {
    console.log("Request Recieved");
    res.send(message);
})

app.listen(8080, () => {
    console.log("Started listening on port: ", 8080);
})
